# Plain Weather Icons for Tempest

Screenshot of main display of the tempest program with the 'flat_colorful'
icon set from this project.

![screenshot](screenshot.jpeg)

## To use with tempest weather program

* Go to the top-level of your [tempest](https://gitlab.com/tempest-weather/tempest) checkout and run the following

        git clone https://gitlab.com/tempest-weather/plain_weather icons/plain_weather

* edit your `config.py` file and change `ICONS_DIR` variable to
  `icons/plain_weather`
* Restart tempest, it should start using the new icons

There are multiple icon sets included here.  You can change which is
used by editing the included `iconmap.json` and changing the "dir" field
near the top to point to the desired directory.  The default is "colorful"

### Sample icons from different sets
<table>
<tr>
    <th>colorful</th>
    <th>flat_colorful</th>
    <th>flat_white</th>
    <th>light</th>
</tr>
<tr bgcolor="black">
    <th><img src="colorful/svg/28.svg"></th>
    <th><img src="flat_colorful/svg/28.svg"></th>
    <th><img src="flat_white/svg/28.svg"></th>
    <th><img src="light/svg/28.svg"></th>
</tr>
<tr bgcolor="black">
    <th><img src="colorful/svg/42.svg"></th>
    <th><img src="flat_colorful/svg/42.svg"></th>
    <th><img src="flat_white/svg/42.svg"></th>
    <th><img src="light/svg/42.svg"></th>
</tr>
</table>


## Credits and License

All image files under this directory are from
<https://www.deviantart.com/merlinthered/art/plain-weather-icons-157162192>.
They have not been modified, but only a subset are included here.

## License

This is licensed under
[Creative Commons Attribution-NonCommercial-ShareAlike 3.0
Unported](http://creativecommons.org/licenses/by-nc-sa/3.0/) license.

Note that the [image creator's web
page](https://www.deviantart.com/merlinthered/art/plain-weather-icons-157162192)
says these icons are licensed under the [Creative Commons
Attribution-ShareAlike
3.0](https://creativecommons.org/licenses/by-sa/3.0/us/) license.
However, the meta data in the original .svg files (and the ones here)
say they are licensed under [Creative Commons
Attribution-NonCommercial-ShareAlike 3.0
Unported](http://creativecommons.org/licenses/by-nc-sa/3.0/) license.
Furthermore, some of the comments by the author on that web page seem
to confirm that the latter is the true intended license for these
icons.  Therefore this project will maintain that license here.
